import time
import pandas as pd
import tqdm
from torch.autograd import Variable
import os.path as osp

import torch
from torch import nn
import pdb
import models
from models import cluster_assess,transformation_image

def dual_iterator(ref, other):
    #pdb.set_trace()
    other_it = zip(other)
    for data in ref:
        data[0] = torch.cat([data[0],data[0],data[0]],dim = 1)

        try:
            data_, = next(other_it)
        except StopIteration:
            other_it = zip(other)
            data_,   = next(other_it)


        yield data, data_
def normLoss(x):
    x = torch.matmul(x,x)-1
    return torch.mul(x,x)
def CrossELoss(x,y):
    crossloss = 0
    for i in range(len(x)):
        crossloss-=x[i]*(torch.log(y[i])/torch.log(torch.Tensor([2])))
    return crossloss

def TransformLoss(x,y,z,t):
    k = abs(1-torch.matmul(x,y)-CrossELoss(z,t))
    return k

def fit(model, optim, dataset,
        n_epochs=10, walker_weight = 1., visit_weight = .1,
        savedir='./log', cuda=None):

    Centroid = Variable(torch.randn(10,128), requires_grad=True)
    yc = torch.Tensor([0,1,2,3,4,5,6,7,8,9])
    cudafy = lambda x : x if cuda is None else x.cuda(cuda)
    torch2np = lambda x : x.cpu().detach().numpy()

    DA_loss  = models.AssociativeLoss(walker_weight=walker_weight, visit_weight=visit_weight)
    CL_loss  = nn.CrossEntropyLoss()
    #pdb.set_trace()
    cudafy(model)
    model.train()

    print('training start!')
    start_time = time.time()
    train_hist = []
    pbar_epoch = tqdm.tqdm(range(n_epochs))
    tic = time.time()
    for epoch in pbar_epoch:
        num_iter = 0
        D_losses = []
        G_losses = []
        epoch_start_time = time.time()
        #pdb.set_trace()

        for (xs, ys) in dataset:
            batch_size = len(xs)
            
            if(xs.size(1)==1):
                xs = torch.cat([xs,xs,xs],dim = 1)
            xs = cudafy(xs)
            ys = cudafy(ys)
            
            losses = {}
            model.zero_grad()

            xa = transformation_image(xs)
            yta = torch.zeros(4*batch_size).int()
            for i in range(4*batch_size):
                yta[i] = i/4


            phi_s, yp   = model(xs)
            phi_a, ya   = model(xa)

            yp  = yp.squeeze().clone()

            losses['D Centroid'] = DA_loss(Centroid,phi_s,yc).mean()
            losses['D adapt'] = DA_loss(phi_a,phi_s,yta).mean()

            transformloss=0
            dem=0
            for l in range(batch_size):
                for t in range(4): 
                    transformloss+= TransformLoss(phi_s[l],phi_a[4*l+t],yp[l],ya[4*l+t])
            losses['D Transform']=transformloss/(4*batch_size)
            #pdb.set_trace()
            normloss = 0
            for demc in range(10):
                normloss+=normLoss(Centroid[demc])
            for l in range(batch_size):
                normloss+= normLoss(phi_s[l])+normLoss(phi_a[4*l])+normLoss(phi_a[4*l+1])+normLoss(phi_a[4*l+2])+normLoss(phi_a[4*l+3])
            losses['D Norm']=normloss/(10+5*batch_size)
                

            (losses['D adapt'] + losses['D Centroid'] + losses['D Transform'] + losses['D Norm']).backward()
            #(losses['D class'] + losses['D adapt'] + losses['D Centroid']).backward()
            optim.step()

            print('Epoch {}, Iteration {} - Accuracy {:.3f} % '.format(epoch, num_iter,100*cluster_assess(yp.max(dim=1)[1].numpy(),ys.numpy(),10)))
            num_iter+=1

            # demtong = 0
            # for t1 in range(10):
            #     dempt = []
            #     demmax = 1
            #     max1 = 0
            #     for t2 in range(100): 
            #         if(yp.max(dim=1)[1][t2]==t1):
            #             dempt.append(ys[t2])
            #     if(len(dempt) ==0):
            #         continue
            #     dempt = torch.stack(dempt)
            #     a,b = torch.sort(dempt)
            #     for chiso in range(len(a)-1):
            #         if(a[chiso]==a[chiso+1]):
            #             demmax+=1
            #             max1 = max(max1,demmax)
            #         else:
            #             demmax = 1
            #     demtong+=max1
            # print('acc hien tai: ',demtong/100)

            # linear assignment, sklearn - acc unsup


            #if num_iter % 10 == 0:

                #print('Epoch {}, Iteration {} - S {:.3f} % - T {:.3f} %'.format(epoch, num_iter,acc_s*100,acc_t*100))


        # epoch_end_time = time.time()
        # per_epoch_ptime = epoch_end_time - epoch_start_time
