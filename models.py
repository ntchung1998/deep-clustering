import h5py
import torch
from torch import nn

import torch.nn.functional as F
import numpy as np
from sklearn.utils.linear_assignment_ import linear_assignment
import cv2
import pdb


def transformation_image(Images):
    Images = Images.numpy()
    dai,ch, rows, cols = Images.shape
    # np.save('fnumpys.npy', Images)
    Images = Images.transpose(0,2,3,1)
    # np.save('fnumpy.npy', Images)
    for i in range(len(Images)):

        image = Images[i]
        pts1 = np.float32([[4, 4], [30, 4], [4, 30]])
        pts2 = np.float32([[3, 9], [25, 10], [14, 28]])
        M = cv2.getAffineTransform(pts1, pts2)
        image1 = cv2.warpAffine(image, M, (cols, rows))
        image1 = np.expand_dims(image1, axis=0)
        
        pts1 = np.float32([[4, 4], [30, 4], [4, 30]])
        pts2 = np.float32([[5, 12], [28,10], [10, 25]])
        M = cv2.getAffineTransform(pts1, pts2)
        image2 = cv2.warpAffine(image, M, (cols, rows))
        image2 = np.expand_dims(image2, axis=0)

        pts1 = np.float32([[4, 4], [30, 4], [4, 30]])
        pts2 = np.float32([[5, 12], [28,6], [5, 20]])
        M = cv2.getAffineTransform(pts1, pts2)
        image3 = cv2.warpAffine(image, M, (cols, rows))
        image3 = np.expand_dims(image3, axis=0)
        
        pts1 = np.float32([[4, 4], [30, 4], [4, 30]])
        pts2 = np.float32([[10, 5], [20,6], [5, 27]])
        M = cv2.getAffineTransform(pts1, pts2)
        image4 = cv2.warpAffine(image, M, (cols, rows))
        cv2.imshow('test',image4)
        cv2.waitKey(0)
        image4 = np.expand_dims(image4, axis=0)
        output = np.concatenate((image1,image2,image3,image4),axis = 0)
        output = output.transpose(0,3,1,2)
        if(i ==0):
            output1 = output
        else:
            output1 = np.concatenate((output1,output),axis = 0)
    #pdb.set_trace()
    output1 = torch.from_numpy(output1)


    return output1


def _hungarian_match(flat_preds, flat_targets, num_k):
  
    num_samples = flat_targets.shape[0]
    num_correct = np.zeros((num_k, num_k))

    for c1 in range(num_k):
        for c2 in range(num_k):
            # elementwise, so each sample contributes once
            votes = int(((flat_preds == c1) * (flat_targets == c2)).sum())
            num_correct[c1, c2] = votes

    # num_correct is small
    match = linear_assignment(num_samples - num_correct)

    # return as list of tuples, out_c to gt_c
    res = []
    for out_c, gt_c in match:
        res.append((out_c, gt_c))

    return res

def _acc(preds, targets, num_k):  
    assert (preds.shape == targets.shape)
    assert (preds.max() < num_k and targets.max() < num_k)
    acc = int((preds == targets).sum()) / float(preds.shape[0])
    return acc

def cluster_assess(preds, targets, num_k):
    match = _hungarian_match(preds, targets, num_k)
    # reorder predictions to be same cluster assignments as gt_k
    n = preds.shape[0]
    reordered_preds = np.zeros(n)
    for pred_i, target_i in match:
        reordered_preds[preds == pred_i] = target_i
            
    acc = _acc(reordered_preds, targets, num_k)
    return acc


class WalkerLoss(nn.Module):

    def forward(self, Psts, y):
        equality_matrix = torch.eq(y.clone().view(-1,1), y).float()
        p_target = equality_matrix / equality_matrix.sum(dim=1, keepdim=True)
        p_target.requires_grad = False

        L_walker = F.kl_div(torch.log(1e-8 + Psts), p_target, size_average=False)
        L_walker /= p_target.size()[0]

        return L_walker

class VisitLoss(nn.Module):

    def forward(self, Pt):
        p_visit = torch.ones([1, Pt.size()[1]]) / float(Pt.size()[1])
        p_visit.requires_grad = False
        if Pt.is_cuda: p_visit = p_visit.cuda()
        L_visit = F.kl_div(torch.log(1e-8 + Pt), p_visit, size_average=False)
        L_visit /= p_visit.size()[0]

        return L_visit

class AssociationMatrix(nn.Module):

    def __init__(self):
        super(AssociationMatrix, self).__init__()

    def forward(self, xs, xt):
        """
        xs: (Ns, K, ...)
        xt: (Nt, K, ...)
        """

        # TODO not sure why clone is needed here
        Bs = xs.size()[0]
        Bt = xt.size()[0]

        xs = xs.clone().view(Bs, -1)
        xt = xt.clone().view(Bt, -1)

        W = torch.mm(xs, xt.transpose(1,0))

        # p(xt | xs) as softmax, normalize over xt axis
        Pst = F.softmax(W, dim=1) # Ns x Nt
        # p(xs | xt) as softmax, normalize over xs axis
        Pts = F.softmax(W.transpose(1,0), dim=1) # Nt x Ns

        # p(xs | xs)
        Psts = Pst.mm(Pts) # Ns x Ns

        # p(xt)
        Pt = torch.mean(Pst, dim=0, keepdim=True) # Nt

        return Psts, Pt

class AssociativeLoss(nn.Module):

    def __init__(self, walker_weight = 1., visit_weight = 1.):
        super(AssociativeLoss, self).__init__()

        self.matrix = AssociationMatrix()
        self.walker = WalkerLoss()
        self.visit  = VisitLoss()

        self.walker_weight = walker_weight
        self.visit_weight  = visit_weight

    def forward(self, xs, xt, y):

        Psts, Pt = self.matrix(xs, xt)
        L_walker = self.walker(Psts, y)
        L_visit  = self.visit(Pt)

        #return self.visit_weight*L_visit + self.walker_weight*L_walker
        return self.walker_weight*L_walker

def conv2d(m,n,k,act=True):
    layers =  [nn.Conv2d(m,n,k,padding=1)]

    if act: layers += [nn.ELU()]

    return nn.Sequential(
        *layers
    )

class SVHNmodel(nn.Module):

    """
    Model for application on SVHN data (32x32x3)
    Architecture identical to https://github.com/haeusser/learning_by_association
    """

    def __init__(self):

        super(SVHNmodel, self).__init__()

        self.features = nn.Sequential(
            nn.InstanceNorm2d(3),
            conv2d(3,  32, 3),
            conv2d(32, 32, 3),
            conv2d(32, 32, 3),
            nn.MaxPool2d(2, 2, padding=0),
            conv2d(32, 64, 3),
            conv2d(64, 64, 3),
            conv2d(64, 64, 3),
            nn.MaxPool2d(2, 2, padding=0),
            conv2d(64, 128, 3),
            conv2d(128, 128, 3),
            conv2d(128, 128, 3),
            nn.MaxPool2d(2, 2, padding=0)
        )

        self.classifier = nn.Sequential(
            nn.Linear(128*4*4, 10)
        )

    def forward(self, x):

        phi  = self.features(x)
        phi_mean = phi.view(-1, 128, 16).mean(dim=-1)
        phi = phi.view(-1,128*4*4)
        y = self.classifier(phi)

        return phi_mean, y


class FrenchModel(nn.Module):

    """
    Model used in "Self-Ensembling for Visual Domain Adaptation"
    by French et al.
    """

    def __init__(self):

        super(FrenchModel, self).__init__()

        def conv2d_3x3(inp,outp,pad=1):
            return nn.Sequential(
                nn.Conv2d(inp,outp,kernel_size=3,padding=pad),
                nn.BatchNorm2d(outp),
                nn.ReLU()
            )

        def conv2d_1x1(inp,outp):
            return nn.Sequential(
                nn.Conv2d(inp,outp,kernel_size=1,padding=0),
                nn.BatchNorm2d(outp),
                nn.ReLU()
            )

        def block(inp,outp):
            return nn.Sequential(
                conv2d_3x3(inp,outp),
                conv2d_3x3(outp,outp),
                conv2d_3x3(outp,outp)
            )

        self.features = nn.Sequential(
            block(3,128),
            nn.MaxPool2d(2, 2, padding=0),
            nn.Dropout2d(p=0.5),
            block(128,256),
            nn.MaxPool2d(2, 2, padding=0),
            nn.Dropout2d(p=0.5),
            conv2d_3x3(256, 512, pad=0),
            conv2d_1x1(512, 256),
            conv2d_1x1(256, 128),
            nn.AvgPool2d(6, 6, padding=0)
        )

        self.classifier = nn.Linear(128,10)

    def forward(self, x):

        phi  = self.features(x)
        phi = phi.view(-1,128)
        # print(x.size(), phi.size())
        y = self.classifier(phi)
        y = F.softmax(y, dim = 1)

        return phi, y
